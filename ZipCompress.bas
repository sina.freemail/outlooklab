Attribute VB_Name = "ZipCompress"

    ' 下記の 1 行はマクロ ファイルの先頭にコピーしてください。
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

    ' ZIP 圧縮を行う VBA マクロ
    Public Sub ZipCompressAttachment( ByVal Item As Object )
        Dim objShell ' As Shell32.Shell
        Dim objFS ' As Scripting.FileSystemObject
        Dim strTemp As String
        Dim fldTemp ' As Scripting.Folder
        Dim objItem ' As MailItem
        Dim i As Integer
        Dim objAttach As Attachment
        Dim strAttach 'As String
        Dim strEmptyZip 'As String
        Dim strZipFile 'As String
        Dim stmZipFile ' As Scripting.TextStream
        Dim fldZip ' As Shell32.Folder
        
        Dim oWshShell

        Dim ZipExe As String
        Dim PWString As String
        
        ZipExe = "d:\zip300xn\zip.exe"  ' info-ZIP からのダウンロード http://infozip.sourceforge.net/
        PWString = "hogehoge"         '　パスワード
        
        
        ' CUI zip.exe があるか確認
        If Dir(ZipExe) <> "" Then
        Else
            MsgBox ("ZipExeが存在しません")
            Exit Sub
        End If
    
        ' SHELL オブジェクトと FileSystemObject オブジェクトを生成
        Set objFS = CreateObject("Scripting.FileSystemObject")
        Set objShell = CreateObject("Shell.Application")
        ' 作業フォルダの作成 (%TEMP% で指定される一時フォルダの下にランダムな名前のフォルダを作成)
        strTemp = objFS.GetSpecialFolder(2) & "\" & objFS.GetTempName()
        Set fldTemp = objFS.CreateFolder(strTemp)
        ' 作成中のアイテムを取得
        Set objItem = Item
        ' 作成中のアイテムを一旦保存
        objItem.Save
        ' 添付ファイルの一つ一つについてチェック
        For i = objItem.Attachments.Count To 1 Step -1
            Set objAttach = objItem.Attachments.Item(i)
            ' 既に ZIP で圧縮済みなら圧縮しない
            If Not objAttach.FileName Like "*.zip" And objAttach.Type = olByValue Then
                ' 作業フォルダに作成するファイルの名前を取得
                strAttach = strTemp & "\" & objAttach.FileName
    
                'Zipファイルが無いときは新たに生成
                If strZipFile = 0 Then
                    'strZipFile = strAttach & ".zip"
                    strZipFile = strTemp & "\" & "添付ファイル.zip"
                    'strZipFile = strTemp & "\" & objFS.GetTempName() & ".zip"
                    
                    
                ' 作業フォルダに添付ファイルを保存
                objAttach.SaveAsFile strAttach
                ' 空の ZIP ファイルを作成
                Call Shell(ZipExe & " -P " & PWString & " -j " & strZipFile & " " & strAttach) '
                
                'Zipファイルができてるか確認
                Do
                    DoEvents
                    Sleep 5000
                    Err.Clear
                Loop While Dir(strZipFile) = ""
                
                ' 作業フォルダのファイルを削除
                objFS.DeleteFile strAttach

                ' 添付ファイルと ZIP ファイルを入れ替え
                objAttach.Delete
            
            '既存のZipファイルがあるときは追加
            Else
          
                ' 作業フォルダに添付ファイルを保存
                objAttach.SaveAsFile strAttach
          
                'Zipファイルに追加（同期処理）
                Set oWshShell = CreateObject("Wscript.Shell")
                oWshShell.Run ZipExe & " -P " & PWString & " -j " & strZipFile & " -m " & strAttach, 0, True
                
                '添付ファイル削除
                objAttach.Delete
            
            End If

        End If
     
    Next
    
    'Zipファイルがあれば添付ファイルに追加
    If strZipFile <> 0 Then
                If objAttach.Position = 0 Then
                    objItem.Attachments.Add strZipFile, olByValue
                Else
                    objItem.Attachments.Add strZipFile, olByValue, objAttach.Position
                End If
        
            '作業フォルダのZipファイルを削除
            objFS.DeleteFile strZipFile
        End If
        
        ' 作業フォルダを削除
        Dir (vbNullString)  ' ロックを解除 http://takyuma.jugem.jp/?eid=23
        fldTemp.Delete
    End Sub
