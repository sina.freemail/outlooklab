VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ThisOutlookSession"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True


' Private WithEvents MyOutlookItems As Outlook.Items


Private Sub Application_ItemSend(ByVal Item As Object, Cancel As Boolean)

    '１．添付ファイル圧縮確認
    Dim objItem ' As MailItem
    Dim ZipfileNum ' File num
    
    Set objItem = Item
    If objItem.Attachments.Count <> 0 Then
         
         ' Zip file 以外のファイルの数を数える
        ZipfileNum = 0
        For i = objItem.Attachments.Count To 1 Step -1
            Set objAttach = objItem.Attachments.Item(i)
            
            If objAttach.Type = olByValue Then
                If Not objAttach.FileName Like "*.zip" Then
                    ZipfileNum = ZipfileNum + 1
                End If
            End If
        Next
        
        ' Zip file 以外のファイルが一つ以上ある場合
        If ZipfileNum > 0 Then
            If MsgBox("圧縮しますか？", vbYesNo) = vbYes Then

                Call ZipCompressAttachment(Item)

            End If
        End If
    End If


    '２．送信確認
    If MsgBox("送信してもいいですか？", vbYesNo) = vbNo Then
        Cancel = True
        Exit Sub
    End If


    '３．送信時BCCに自分のアドレス追加
    Dim objMe As Recipient
    Set objMe = Item.Recipients.Add( Item.SendUsingAccount.SmtpAddress ) 'https://docs.microsoft.com/ja-jp/office/vba/api/outlook.mailitem.sendusingaccount'
    objMe.Type = olBCC
    
    If Not objMe.Resolve Then
        strMsg = "Could not resolve the Bcc recipient. " & _
                "Do you want still to send the message?"
        res = MsgBox(strMsg, vbYesNo + vbDefaultButton1, _
                "Could Not Resolve Bcc Recipient")
        If res = vbNo Then
            Cancel = True
        End If
    End If
    
    Set objMe = Nothing
    
End Sub


